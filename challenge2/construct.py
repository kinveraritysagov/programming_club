import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt

x = np.arange(2, 16, 1)
print(x)

cols = 100

xy = np.ndarray((len(x), cols + 1))

headers = ["x"] + ["y%d" % i for i in range(cols)]
xy[:, 0] = x
for i in range(cols):
    m, c = np.random.rand(2)
    m *= 2
    m -= 1
    noise = (np.random.rand(len(x)) * 2)
    
    y = m * x + c
    y *= noise
    
    # print("m=%s c=%s" % (m, c))
    # print("noise=%s" % (noise, ))
    # plt.gca().set_aspect("equal")
    # plt.plot(x, y, ls="none", marker="o")
    # plt.show()

    xy[:, i + 1] = y


np.savetxt("data2.csv", xy, fmt="%s", delimiter=",", header=",".join(headers), comments="")