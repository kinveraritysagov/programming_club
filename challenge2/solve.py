import numpy as np 
import matplotlib.pyplot as plt
import seaborn as sns

# --------------- Part1 1 ----------------------

# Load the CSV file
# The argument skiprows=1 avoids the header row, which numpy.loadtxt doesn't like.
data = np.loadtxt("data.csv", delimiter=",", skiprows=1) 


# data is a 2D numpy array. We want to select the first column
# as the variable x, and the second column as the variable y.
x = data[:, 0]
y = data[:, 1]

# Create a new array for x, for each value of which we will plot
# the straight line against.
xfwd = np.linspace(min(x), max(x), 50)

# Fit a polynomial of 1-th degree (see the third argument) to the x and y arrays.
m, c = np.polyfit(x, y, 1)

# Calculate the value of the polynomial at each of our x coordinates in the xfwd array.
yfwd = m * xfwd + c

# Create a label for the polynomial equation.
label = "y = %.2f * x + %.2f" % (m, c)

# Create a new figure
plt.figure()

# Draw the original data as points.
plt.plot(x, y, marker='o', ls='none', label='Original data')

# Draw the polynomial as a line without points.
plt.plot(xfwd, yfwd, color='r', label=label)

# Label the axes and show the legend with labels.
plt.xlabel("x")
plt.ylabel("y")
plt.legend()

# Manually place a label on the graph with the polynomial's equation.
plt.text(11, 8.8, label, color="r", rotation=30, ha="left", va="bottom")

# Show the graph.
plt.show()

# --------------- Part 2 ----------------------

# Load the data2.csv file.
data = np.loadtxt("data2.csv", delimiter=",", skiprows=1)

# Load the first column as the x array.
x = data[:, 0]      

plt.figure()

colours = sns.color_palette(n_colors=data.shape[1] - 1)

# Iterate through each column in the array (skipping the first column)
for i in range(1, data.shape[1]):

    # This time the y variable is the i-th column of the 2D array.
    y = data[:, i]

    xfwd = np.linspace(min(x), max(x), 50)
    m, c = np.polyfit(x, y, 1)
    yfwd = m * xfwd + c
    label = "y%d = %.2f * x + %.2f" % (i - 1, m, c)
    print(label)

    colour = colours[i - 1]
    plt.plot(x, y, marker='.', color=colour, ls='none')
    plt.plot(xfwd, yfwd, color=colour)

plt.xlabel("x")
plt.ylabel("y")
plt.show()