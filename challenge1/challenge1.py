import pandas as pd

df = pd.read_csv("Challenge1.csv")
df2 = pd.DataFrame(dict(id=df["ID"], sum=df["Column1"] + df["Column2"]))
df2.to_csv("Challenge Accepted.csv", index=False)

msg = []
for item in df2["sum"].values:
    msg.append(chr(int(str(item), 2)))

print(''.join(msg))